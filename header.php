<?php 
    //Запускаем сессию
    session_start();

    //Добавляем файл подключения к БД
    require_once("dbconnect.php");

    if(isset($_COOKIE["password_cookie_token"]) && !empty($_COOKIE["password_cookie_token"])){

        $select_user_data = $mysqli->query("SELECT email, password FROM `users` WHERE password_cookie_token = '".$_COOKIE["password_cookie_token"]."'");

        if(!$select_user_data){
            echo "<p class='mesage_error' >Ошибка выборки БД.</p>".$mysqli->error();
        }else{

            $array_user_data = $select_user_data->fetch_array(MYSQLI_ASSOC);

            if($array_user_data){
                
                $_SESSION['email'] = $array_user_data["email"];
                $_SESSION['password'] = $array_user_data["password"];

            }
        }

    }

?>
<!DOCTYPE html>
<html>
<head>
    <title>Название нашего сайта</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<div id="header">
    <h2>Шапка сайта</h2>

    <a href="/index.php">Главная</a>

    <div id="auth_block">
    <?php 
        //Проверяем, авторизован ли пользователь
        if(!isset($_SESSION['email']) && !isset($_SESSION['password'])){
            // если нет, то выводим блок со ссылками на страницу регистрации и авторизации
    ?>
            <div id="link_register">
                <a href="form_register.php">Регистрация</a>
            </div>

            <div id="link_auth">
                <a href="form_auth.php">Авторизация</a>
            </div>
    <?php
        }else{
            //Если пользователь авторизован, то выводим ссылку Выход
    ?>  
            <div id="link_logout">
                <a href="logout.php">Выход</a>
            </div>
    <?php
        }
    ?>
    </div>
     <div class="clear"></div>
</div>

